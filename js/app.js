const setLaunchDay = () => {
  let date = new Date();
  date.setDate(date.getDate() + 14);
  return date;
};

const launchDay = new Date(setLaunchDay());

let previousTimeBetweenDates;

setInterval(() => {
  let currentDate = new Date();

  const timeBetweenDates = Math.ceil(currentDate - launchDay);

  if (timeBetweenDates !== previousTimeBetweenDates) {
    flipAllCards(timeBetweenDates);
  }

  previousTimeBetweenDates = timeBetweenDates;
}, 250);

const flipCard = document.querySelector(".flip-card");

function flipAllCards(distance) {
  let days = Math.floor(distance / (1000 * 60 * 60 * 24));
  let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  let seconds = Math.floor((distance % (1000 * 60)) / 1000);

  const dataDays = document.querySelector("[data-days]")
  const dataHours = document.querySelector("[data-hours]")
  const dataMinutes = document.querySelector("[data-minutes]")
  const dataSecondes = document.querySelector("[data-secondes]")

  flip(dataDays, (days * -1))
  flip(dataHours, (hours * -1))
  flip(dataMinutes, (minutes * -1))
  flip(dataSecondes, (seconds * -1))
}


function flip(flipCard, newNumber) {
  const cardTop = flipCard.querySelector(".card-top");
  let currentNumber = parseInt(cardTop.textContent);
  if (currentNumber === newNumber) return;

    const cardBottom = flipCard.querySelector(".card-bottom");
    const topFlip = document.createElement("p");
    topFlip.classList.add("top-flip");
    const bottomFlip = document.createElement("p");
    bottomFlip.classList.add("bottom-flip");

    cardTop.textContent = addZero(currentNumber);
    cardBottom.textContent = addZero(currentNumber)
    topFlip.textContent = addZero(currentNumber);
    bottomFlip.textContent = addZero(newNumber);
    topFlip.addEventListener("animationstart", (e) => {
      cardTop.textContent = addZero(newNumber);
    });
  
    topFlip.addEventListener("animationend", (e) => {
      topFlip.remove();
    });
  
    bottomFlip.addEventListener("animationend", (e) => {
      cardBottom.textContent = addZero(newNumber);
      bottomFlip.remove();
    });
  
    flipCard.append(topFlip, bottomFlip);
  }
  

  function addZero (number) {
    return number >= 0 && number < 10 ? `0${number}` : number
  }
