#!/usr/bin/env bash

mkdir assets/
mkdir css/
mkdir js/
touch /css/style.css
touch /js/app.js
mv design  ./assets
mv images ./assets
mv style-guide.md ./assets/design
mv README-template.md README.md
git init --initial-branch=main
git add assets index.html README.md .gitignore
git commit -m "initial setup"
git checkout -b dev